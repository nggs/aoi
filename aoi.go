package aoi

type Coord float32

type IAOICallback interface {
	OnEnterAOI(other *AOI)
	OnLeaveAOI(other *AOI)
}

type AOI struct {
	x    Coord
	y    Coord
	dist Coord
	data interface{}

	callback IAOICallback
	implData interface{}

	//// Fields for XZListAOIManager
	//neighbors    AOISet
	//xPrev, xNext *AOI
	//yPrev, yNext *AOI
	//markVal      int
}

func NewAOI(data interface{}, callback IAOICallback) *AOI {
	aoi := &AOI{
		data:     data,
		callback: callback,
	}
	return aoi
}

func (a AOI) X() Coord {
	return a.x
}

func (a AOI) Y() Coord {
	return a.y
}

func (a *AOI) SetDist(dist Coord) {
	a.dist = dist
}

func (a AOI) Dist() Coord {
	return a.dist
}

func (a AOI) Data() interface{} {
	return a.data
}

type IAOIManager interface {
	Enter(aoi *AOI, x, y Coord)
	Leave(aoi *AOI)
	Moved(aoi *AOI, x, y Coord)
}
