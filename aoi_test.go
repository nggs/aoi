package aoi

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"sync/atomic"
	"testing"
	"time"
)

const (
	MIN_X = 0
	MAX_X = 1366
	MIN_Y = 0
	MAX_Y = 1366

	NUM_OBJS = 2000

	AOI_DISTANCE = 1366

	o_x = 0
	o_y = 0
)

func Test(t *testing.T) {
	mgr := NewXZListAOIManager(AOI_DISTANCE)

	o := newTestObj()
	mgr.Enter(o.aoi, o_x, o_y)

	obj := newTestObj()
	mgr.Enter(obj.aoi, o_x, o_y)

	mgr.Moved(obj.aoi, o_x, o_y+AOI_DISTANCE+1)

	mgr.Moved(obj.aoi, o_x, o_y)

	//objs := []*TestObj{}
	//for i := 0; i < NUM_OBJS; i++ {
	//	obj := newTestObj()
	//	obj.aoi = NewAOI(obj, obj)
	//	objs = append(objs, obj)
	//	mgr.Enter(obj.aoi, o_x+500+Coord(i), o_y+500+Coord(i))
	//}
	//
	//var count int
	//for _, obj := range objs {
	//	if obj.NeighborsLen() != NUM_OBJS {
	//		count += 1
	//	}
	//}
	//
	//fmt.Printf("NeighborsLen != NUM_OBJS obj count = %d\n", count)
}

func TestXZListAOIManager(t *testing.T) {
	testAOI(t, "XZListAOI", NewXZListAOIManager(AOI_DISTANCE), NUM_OBJS)
}

//func testTowerAOIManager(t *testing.T) {
//	testAOI(t, "TowerAOI", NewTowerAOIManager(MIN_X, MAX_X, MIN_Y, MAX_Y, 10), NUM_OBJS)
//}

type TestObj struct {
	aoi            *AOI
	ID             int64
	neighbors      map[*TestObj]struct{}
	totalNeighbors int64
	calc           int64
}

var gID int64

func nextID() int64 {
	return atomic.AddInt64(&gID, 1)
}

func newTestObj() *TestObj {
	to := &TestObj{
		ID:        nextID(),
		neighbors: make(map[*TestObj]struct{}),
	}
	to.aoi = NewAOI(to, to)
	return to
}

func (obj *TestObj) OnEnterAOI(otherAOI *AOI) {
	other := obj.getObj(otherAOI)
	if obj == other {
		panic("should not enter self")
	}
	if _, ok := obj.neighbors[other]; ok {
		panic("duplicae enter aoi")
	}
	obj.neighbors[other] = struct{}{}
	obj.totalNeighbors += int64(len(obj.neighbors))
	obj.calc += 1

	if obj.ID == 1 {
		fmt.Printf("aoi[%d] enter aoi[%d]\n", other.ID, obj.ID)
	}
}

func (obj *TestObj) OnLeaveAOI(otherAOI *AOI) {
	other := obj.getObj(otherAOI)
	if obj == other {
		panic("should not leave self")
	}
	if _, ok := obj.neighbors[other]; !ok {
		panic("duplicate leave aoi")
	}
	delete(obj.neighbors, other)
	obj.totalNeighbors += int64(len(obj.neighbors))
	obj.calc += 1

	if obj.ID == 1 {
		fmt.Printf("aoi[%d] leave aoi[%d]\n", other.ID, obj.ID)
	}
}

func (obj TestObj) NeighborsLen() int {
	return len(obj.neighbors)
}

func (obj TestObj) String() string {
	//return fmt.Sprintf("TestObj<%d>, x<%f>, y<%f>, NeighborsLen()<%d>",
	//	obj.ID, obj.aoi.X(), obj.aoi.Y(), obj.NeighborsLen())
	ba, _ := json.Marshal(obj)
	return string(ba)
}

func (obj *TestObj) getObj(aoi *AOI) *TestObj {
	return aoi.Data().(*TestObj)
}

func randCoord(min Coord, max Coord) Coord {
	return min + Coord(rand.Intn(int(max)-int(min)))
}

func testAOI(t *testing.T, aoiMgrName string, aoiMgr IAOIManager, numAOI int) {
	objs := []*TestObj{}
	for i := 0; i < numAOI; i++ {
		obj := newTestObj()
		obj.aoi = NewAOI(obj, obj)
		objs = append(objs, obj)
		aoiMgr.Enter(obj.aoi, randCoord(MIN_X, MAX_X), randCoord(MIN_Y, MAX_Y))
	}

	//proffd, _ := os.OpenFile(aoiMgrName+".pprof", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0666)
	//defer proffd.Close()

	//pprof.StartCPUProfile(proffd)
	for i := 0; i < 10; i++ {
		begin := time.Now()
		for _, obj := range objs {
			aoiMgr.Moved(obj.aoi, obj.aoi.x+randCoord(-AOI_DISTANCE, AOI_DISTANCE), obj.aoi.y+randCoord(-AOI_DISTANCE, AOI_DISTANCE))
		}
		consume := time.Now().Sub(begin)
		t.Logf("%s tick %d objects takes %s", aoiMgrName, numAOI, consume)
	}

	for _, obj := range objs {
		aoiMgr.Leave(obj.aoi)
	}

	//pprof.StopCPUProfile()

	//if VERIFY_NEIGHBOR_COUNT {
	totalCalc := int64(0)
	for _, obj := range objs {
		totalCalc += obj.calc
	}
	println("Average calculate count:", totalCalc/int64(len(objs)))
	//}
}
