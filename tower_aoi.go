package aoi

import "log"

type TowerAOIManager struct {
	minX, maxX, minY, maxY Coord
	towerRange             Coord
	towers                 [][]tower
	xTowerNum, yTowerNum   int
}

func (mgr *TowerAOIManager) Enter(aoi *AOI, x, y Coord) {
	aoi.x, aoi.y = x, y
	obj := &aoiobj{aoi: aoi}
	aoi.implData = obj

	mgr.visitWatchedTowers(x, y, aoi.dist, func(tower *tower) {
		tower.addWatcher(obj)
	})

	t := mgr.getTowerXY(x, y)
	t.addObj(obj, nil)
}

func (mgr *TowerAOIManager) Leave(aoi *AOI) {
	obj := aoi.implData.(*aoiobj)
	obj.tower.removeObj(obj, true)

	mgr.visitWatchedTowers(aoi.x, aoi.y, aoi.dist, func(tower *tower) {
		tower.removeWatcher(obj)
	})
}

func (mgr *TowerAOIManager) Moved(aoi *AOI, x, y Coord) {
	oldx, oldy := aoi.x, aoi.y
	aoi.x, aoi.y = x, y
	obj := aoi.implData.(*aoiobj)
	t0 := obj.tower
	t1 := mgr.getTowerXY(x, y)

	if t0 != t1 {
		t0.removeObj(obj, false)
		t1.addObj(obj, t0)
	}

	oximin, oximax, oyimin, oyimax := mgr.getWatchedTowers(oldx, oldy, aoi.dist)
	ximin, ximax, yimin, yimax := mgr.getWatchedTowers(x, y, aoi.dist)

	for xi := oximin; xi <= oximax; xi++ {
		for yi := oyimin; yi <= oyimax; yi++ {
			if xi >= ximin && xi <= ximax && yi >= yimin && yi <= yimax {
				continue
			}

			tower := &mgr.towers[xi][yi]
			tower.removeWatcher(obj)
		}
	}

	for xi := ximin; xi <= ximax; xi++ {
		for yi := yimin; yi <= yimax; yi++ {
			if xi >= oximin && xi <= oximax && yi >= oyimin && yi <= oyimax {
				continue
			}

			tower := &mgr.towers[xi][yi]
			tower.addWatcher(obj)
		}
	}
}

func (mgr *TowerAOIManager) transXY(x, y Coord) (int, int) {
	xi := int((x - mgr.minX) / mgr.towerRange)
	yi := int((y - mgr.minY) / mgr.towerRange)
	return mgr.normalizeXi(xi), mgr.normalizeYi(yi)
}

func (mgr *TowerAOIManager) normalizeXi(xi int) int {
	if xi < 0 {
		xi = 0
	} else if xi >= mgr.xTowerNum {
		xi = mgr.xTowerNum - 1
	}
	return xi
}

func (mgr *TowerAOIManager) normalizeYi(yi int) int {
	if yi < 0 {
		yi = 0
	} else if yi >= mgr.yTowerNum {
		yi = mgr.yTowerNum - 1
	}
	return yi
}

func (mgr *TowerAOIManager) getTowerXY(x, y Coord) *tower {
	xi, yi := mgr.transXY(x, y)
	return &mgr.towers[xi][yi]
}

func (mgr *TowerAOIManager) getWatchedTowers(x, y Coord, aoiDistance Coord) (int, int, int, int) {
	ximin, yimin := mgr.transXY(x-aoiDistance, y-aoiDistance)
	ximax, yimax := mgr.transXY(x+aoiDistance, y+aoiDistance)
	//aoiTowerNum := int(aoiDistance/mgr.towerRange) + 1
	//ximid, yimid := mgr.transXY(x, y)
	//ximin, ximax := mgr.normalizeXi(ximid-aoiTowerNum), mgr.normalizeXi(ximid+aoiTowerNum)
	//yimin, yimax := mgr.normalizeYi(yimid-aoiTowerNum), mgr.normalizeYi(yimid+aoiTowerNum)
	return ximin, ximax, yimin, yimax
}

func (mgr *TowerAOIManager) visitWatchedTowers(x, y Coord, aoiDistance Coord, f func(*tower)) {
	ximin, ximax, yimin, yimax := mgr.getWatchedTowers(x, y, aoiDistance)
	for xi := ximin; xi <= ximax; xi++ {
		for yi := yimin; yi <= yimax; yi++ {
			tower := &mgr.towers[xi][yi]
			f(tower)
		}
	}
}

func (mgr *TowerAOIManager) init() {
	numXSlots := int((mgr.maxX-mgr.minX)/mgr.towerRange) + 1
	mgr.xTowerNum = numXSlots
	numYSlots := int((mgr.maxY-mgr.minY)/mgr.towerRange) + 1
	mgr.yTowerNum = numYSlots
	mgr.towers = make([][]tower, numXSlots)
	for i := 0; i < numXSlots; i++ {
		mgr.towers[i] = make([]tower, numYSlots)
		for j := 0; j < numYSlots; j++ {
			mgr.towers[i][j].init()
		}
	}
}

func NewTowerAOIManager(minX, maxX, minY, maxY Coord, towerRange Coord) IAOIManager {
	aoiman := &TowerAOIManager{minX: minX, maxX: maxX, minY: minY, maxY: maxY, towerRange: towerRange}
	aoiman.init()

	return aoiman
}

type tower struct {
	objs     map[*aoiobj]struct{}
	watchers map[*aoiobj]struct{}
}

func (t *tower) init() {
	t.objs = map[*aoiobj]struct{}{}
	t.watchers = map[*aoiobj]struct{}{}
}

func (t *tower) addObj(obj *aoiobj, fromOtherTower *tower) {
	obj.tower = t
	t.objs[obj] = struct{}{}
	if fromOtherTower == nil {
		for watcher := range t.watchers {
			if watcher == obj {
				continue
			}
			watcher.aoi.callback.OnEnterAOI(obj.aoi)
		}
	} else {
		// obj moved from other tower to this tower
		for watcher := range fromOtherTower.watchers {
			if watcher == obj {
				continue
			}
			if _, ok := t.watchers[watcher]; ok {
				continue
			}
			watcher.aoi.callback.OnLeaveAOI(obj.aoi)
		}
		for watcher := range t.watchers {
			if watcher == obj {
				continue
			}
			if _, ok := fromOtherTower.watchers[watcher]; ok {
				continue
			}
			watcher.aoi.callback.OnEnterAOI(obj.aoi)
		}
	}
}

func (t *tower) removeObj(obj *aoiobj, notifyWatchers bool) {
	obj.tower = nil
	delete(t.objs, obj)
	if notifyWatchers {
		for watcher := range t.watchers {
			if watcher == obj {
				continue
			}
			watcher.aoi.callback.OnLeaveAOI(obj.aoi)
		}
	}
}

func (t *tower) addWatcher(obj *aoiobj) {
	if _, ok := t.watchers[obj]; ok {
		log.Panicf("duplicate add watcher")
	}
	t.watchers[obj] = struct{}{}
	// now obj can see all objs under this tower
	for neighbor := range t.objs {
		if neighbor == obj {
			continue
		}
		obj.aoi.callback.OnEnterAOI(neighbor.aoi)
	}
}

func (t *tower) removeWatcher(obj *aoiobj) {
	if _, ok := t.watchers[obj]; !ok {
		log.Panicf("duplicate remove watcher")
	}

	delete(t.watchers, obj)
	for neighbor := range t.objs {
		if neighbor == obj {
			continue
		}
		obj.aoi.callback.OnLeaveAOI(neighbor.aoi)
	}
}

type aoiobj struct {
	aoi   *AOI
	tower *tower
}
