package aoi

type xzaoi struct {
	aoi          *AOI
	neighbors    map[*xzaoi]struct{}
	xPrev, xNext *xzaoi
	yPrev, yNext *xzaoi
	markVal      int
}

// XZListAOIManager is an implementation of AOICalculator using XZ lists
type XZListAOIManager struct {
	aoiDistance Coord
	xSweepList  *xAOIList
	zSweepList  *yAOIList
}

func NewXZListAOIManager(aoiDistance Coord) IAOIManager {
	return &XZListAOIManager{
		aoiDistance: aoiDistance,
		xSweepList:  newXAOIList(aoiDistance),
		zSweepList:  newYAOIList(aoiDistance),
	}
}

// Enter is called when Entity enters Space
func (mgr *XZListAOIManager) Enter(aoi *AOI, x, y Coord) {
	xzaoi := &xzaoi{
		aoi:       aoi,
		neighbors: map[*xzaoi]struct{}{},
	}
	aoi.x, aoi.y = x, y
	aoi.implData = xzaoi
	mgr.xSweepList.Insert(xzaoi)
	mgr.zSweepList.Insert(xzaoi)
	mgr.adjust(xzaoi)
}

// Leave is called when Entity leaves Space
func (mgr *XZListAOIManager) Leave(aoi *AOI) {
	xzaoi := aoi.implData.(*xzaoi)
	mgr.xSweepList.Remove(xzaoi)
	mgr.zSweepList.Remove(xzaoi)
}

// Moved is called when Entity moves in Space
func (mgr *XZListAOIManager) Moved(aoi *AOI, x, y Coord) {
	oldX := aoi.x
	oldY := aoi.y
	aoi.x, aoi.y = x, y
	xzaoi := aoi.implData.(*xzaoi)
	if oldX != x {
		mgr.xSweepList.Move(xzaoi, oldX)
	}
	if oldY != y {
		mgr.zSweepList.Move(xzaoi, oldY)
	}
	mgr.adjust(xzaoi)
}

// adjust is called by Entity to adjust neighbors
func (mgr *XZListAOIManager) adjust(aoi *xzaoi) {
	mgr.xSweepList.Mark(aoi)
	mgr.zSweepList.Mark(aoi)
	// AOI marked twice are neighbors
	for neighbor := range aoi.neighbors {
		if neighbor.markVal == 2 {
			// neighbors kept
			neighbor.markVal = -2 // mark this as neighbor
		} else { // markVal < 2
			// was neighbor, but not any more
			delete(aoi.neighbors, neighbor)
			aoi.aoi.callback.OnLeaveAOI(neighbor.aoi)
			delete(neighbor.neighbors, aoi)
			neighbor.aoi.callback.OnLeaveAOI(aoi.aoi)
		}
	}

	// travel in X list again to find all new neighbors, whose markVal == 2
	mgr.xSweepList.GetClearMarkedNeighbors(aoi)
	// travel in Z list again to unmark all
	mgr.zSweepList.ClearMark(aoi)
}
